# (cons 'scheme 'regular-expressions)

This is an implementation of regular expressions for R7RS, it's based on the regular expression derivatives by Janusz Brzozowski and follows the paper *Regular-expression derivatives reexamined* by Scott Owens, John Reppy and Aaron Turon.

This library exports an `rx` macro for constructing regular expressions and procedures for processing them.

## regular expression syntax
The following form is how regular expressions are constructed
```
(rx re)
```
where `re` is a regular expression if:

- `re` is an identifier bound to a regular expression
- `re` is the symbol `eps` (meaning *epsilon* or *empty string*
- `re` is the symbol `nil` (meaning *empty language*)
- `re` is a charset from the library (sets charset)
- `re` is `s` where `s` is a string
- `re` is `(seq r1 ...)` where `r1 ...` are regular expressions (seq means *sequence*)
- `re` is `(alt r1 ...)` where `r1 ...` are regular expressions (alt means *alternation*)
- `re` is `(zom r)` where `r` is a regular expression (zom means *zero or more*)
- `re` is `(and r1 ...)` where `r1 ...` are regular expressions (and means ... well ... *and*)
- `re` is `(not r)` where `r` is a regular expression (not means ... well ... *not*)
- `re` is `(opt r)` where `r` is a regular expression (opt means *optional*)
- `re` is `(oom r)` where `r` is a regular expression (oom means *one or more*)
- `re` is `(nom n r)` where `r` is a regular expression and `n` is a non-negative integer (nom means *n or more*)
- `re` is `(ztn n r)` where `r` is a regular expression and `n` is a non-negative integer (ztn means *zero to n*)
- `re` is `(ntm n m r)` where `r` is a regular expression, `n` and `m` are non-negative integers and `m`>=`n` (ntm means *n to m repetitions*)

## regular expression procedures

This implementation consists of the syntax for regular expressions, a simple matching procedure called `regex-match` and some predefined common regular expressions ready to be used.

The `regex-match` procedure takes a regular expression built with rx and a sequence of characters object, there are two additional and optional indices that can be provided for trying to match the obj starting from the *n-th* character and for stoping the matching at the *m-th* character. The sequence of characters object that this implementation supports are lists of characters, strings and streams (using the `(streams)` library).

The following regular expressions are exported by the library:
- `rx:ascii`/`rx:any` Match any ASCII character
- `rx:nonl` Match any character other than #\return or #\newline
- `rx:lower-case`/`rx:lower` Matches any character in the range `#\a` to `#\z`
- `rx:upper-case`/`rx:upper` Matches any character in the range `#\A` to `#\Z`
- `rx:alphabetic`/`rx:alpha` Matches any character in the range `#\a` to `#\z` or `#\A` to `#\Z`
- `rx:numeric`/`rx:num` Matches any character in the range `#\0` to `#\9`
- `rx:alphanumeric`/`rx:alphanum`/`rx:alnum` Matches any character which is `rx:alphabetic` or `rx:numeric`
- `rx:punctuation`/`rx:punct` Matches any punctuation character, that is a char in `"!\"#%&'()*,-./:;?@[\]_{}"`
- `rx:symbol` Matches any symbol character, that is, one of  `$`, `+`, `<`, `=`, `>`, `^`, the backtick, `|` or `~`
- `rx:graphic`/`rx:graph` Matches any character that is `rx:alphanumeric`, `rx:punctuation` or `rx:symbol`
- `rx:whitespace`/`rx:white`/`rx:space` Matches any whitespace character, that is a space, tab, line feed, form feed, or carriage return
- `rx:printing`/`rx:print` Matches any printing character that is `rx:graphic` or `rx:whitespace`
- `rx:control`/`rx:cntrl` Matches any control or other character, corresponding to the characters corresponding to the ascii range 0 to 31
- `rx:hex-digit`/`rx:xdigit` Matches any valid digit in hexadecimal notation that is `rx:numeric` or the letters `a`, `b`, `c`, `d`, `e`, `f`, `A`, `B`, `C`, `D`, `E` or  `F`

An extra utility procedure `regex->list` is provided for pretty-printing in the repl.
